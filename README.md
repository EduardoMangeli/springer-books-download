# Pre-install
- Install `chromedriver` 

    https://chromedriver.chromium.org/

# Install
- Put all repo files in the same folder

- Install libs

    `pip install -r requirements.txt`

# Config
- You can choose the _books destination folder_ modifying _books_folder_ variable value at the file `downloader.py`
    
    `books_folder = "books_n"`

# Run
`python downloader.py` 