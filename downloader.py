from openpyxl import load_workbook
from selenium import webdriver
from urllib.request import urlretrieve
from os import path
from pathlib import Path
from progressbar import ProgressBar

# create driver
driver = webdriver.Chrome()

# books destination folder
books_folder = "books_n"

pbar = None


def download_book(url, name):
    print(url)
    driver.get(url)
    assert name[:4] in driver.title
    try:
        pdf_button = driver.find_elements_by_class_name("test-bookpdf-link")[0]
        if not path.isfile(books_folder + '/' + name.replace('/', '-') + '.pdf'):
            urlretrieve(pdf_button.get_attribute('href'),
                        filename=books_folder + '/' + name.replace('/', '-') + '.pdf', reporthook=show_progress)
    except Exception as err:
        print("Erro:", err, name)
    if driver.find_elements_by_class_name("test-bookepub-link"):
        try:
            epub_button = driver.find_elements_by_class_name("test-bookepub-link")[0]
            if not path.isfile(books_folder + '/' + name.replace('/', '-') + '.epub'):
                urlretrieve(epub_button.get_attribute('href'),
                            filename=books_folder + '/' + name.replace('/', '-') + '.epub', reporthook=show_progress)
        except Exception as err:
            print("Erro:", err, name)


def get_free_books():
    # open file
    wb = load_workbook("Free+English+textbooks.xlsx")

    # select worksheet
    ws = wb["eBook list"]

    # get urls
    urls = ws['S']
    names = ws['A']

    # iterate through urls
    for index in range(len(urls)):
        if index > 0:
            print('Free books:', index, "/", len(urls))
            download_book(urls[index].value, names[index].value)


def get_open_access():
    page_url = 'https://link.springer.com/search/page/45?facet-content-type="Book"&package=openaccess&facet-language="En"'

    driver.get(page_url)
    total_books = driver.find_element_by_css_selector('#number-of-search-results-and-search-terms > strong').text
    books_counter = 1

    ended = False
    while not ended:
        driver.get(page_url)
        assert "Search Results - Springer" in driver.title
        books = make_books_list(driver.find_elements_by_css_selector('a.title'))
        for href, name in books:
            print('Open access:', books_counter, "/", total_books)
            download_book(href, name)
            books_counter += 1

        driver.get(page_url)  # one step back to the current books list page
        next_button = driver.find_element_by_css_selector('.next > img')
        if next_button.find_element_by_xpath("..").tag_name == 'a':
            next_button.click()
            page_url = driver.current_url
        else:
            ended = True


def make_books_list(elements_list):
    return [(el.get_attribute('href'), el.text) for el in elements_list]


def show_progress(block_num, block_size, total_size):
    global pbar

    if pbar is None:
        pbar = ProgressBar(maxval=total_size)

    downloaded = block_num * block_size
    if downloaded < total_size:
        pbar.update(downloaded)
    else:
        pbar.finish()
        pbar = None


if __name__ == '__main__':
    Path(books_folder).mkdir(parents=True, exist_ok=True)
    get_free_books()
    get_open_access()
